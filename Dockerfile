FROM registry.gitlab.com/sentinelc/containers/alpine:3.17.0

RUN apk add tshark

ENTRYPOINT ["tshark", "-i", "eth0"]
